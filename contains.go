package main

import (
	"fmt"
	"strings"
)

func main() {
	fmt.Println("You find yourself in a dimly lit cavern.")
	var command = "walk outside"
	var exit = strings.Contains(command, "outside")
	fmt.Println("You leave the cave:", exit)
	fmt.Println("Emerging from the cave, your eyes meet the blinding midday sun.")
	var wearShades = true
	fmt.Println("You should wear the shades:", wearShades)
	fmt.Println("There is a sign near the cave entrance.")
	var sign = "Warning: You should read this!"
	var shouldRead = strings.Contains(sign, "read")
	fmt.Println("You should read the text on sing at cave entrance:", shouldRead)
}

package main

import (
	"fmt"
	"math/rand"
)

func main() {
	var number = 21
	for {
		var guess = rand.Intn(100) + 1
		if guess == number {
			fmt.Printf("You guessed the number %v\n", guess)
			break
		} else if guess > number {
			fmt.Println("The number is too big!")
		} else {
			fmt.Println("The number is too small!")
		}
	}
}

// Display text "Hello, world!" on console output
package main

import (
	"fmt"
)

func main() {
	fmt.Println("Hello, world!")
}

package main

import "fmt"

func main() {
	var command = "go east"
	// if command is equal to "go east"
	if command == "go east" {
		fmt.Println("You head further up the mountain.")
		// otherwise, if command is equal to "go inside"
	} else if command == "go inside" {
		fmt.Println("You enter the cave where you live out the rest of your life.")
		// or, if anything else
	} else {
		fmt.Println("Didn't quite get that.")
	}
}

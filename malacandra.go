package main

import "fmt"

func main() {
	const (
		distance     = 56000000 // km
		tripDuration = 28       // days
		hoursPerDay  = 24
	)

	var spaceShipSpeed = distance / (tripDuration * hoursPerDay)
	fmt.Println(spaceShipSpeed, " km/h")
}

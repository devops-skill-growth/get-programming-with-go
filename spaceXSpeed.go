// How long does it take to get to Mars?
// SpaceX Inter Planetary System will cruze at the
// respectable speed of 100,800 km/h, while earth-mars
// distance will be 96,300,000 km. Calculate trip in days.
package main

import "fmt"

func main() {
	const spaceXIPTSSpeed = 100800 // km/h
	const hoursPerEarthDay = 24    // h
	var distance = 96300000        // km
	fmt.Println(distance/spaceXIPTSSpeed/hoursPerEarthDay, "days")
}
